/*
  *Nombre archivo: action.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha creación: Septiembre de 2011
  *Fecha última actualización: Enero del 2012
  *Descripción: La clase action representa la jugada que una ficha hace en un estado del juego, los atributos
  que la componen son el id del la ficha que realiza la accion  y la posicion donde se mueve la ficha.

  *Universidad del Valle
  */


#ifndef ACTION_H
#define ACTION_H

#include <QPoint>
#include <QString>

class Action
{
public:
    Action();
    Action(char id, QPoint position);
    void setId(char id);
    void setPosition(QPoint position);
    char getId();
    QPoint getPosition();
    QString toString();
private:
    char id;
    QPoint position;
};

#endif // ACTION_H
