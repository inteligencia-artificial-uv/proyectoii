#-------------------------------------------------
#
# Project created by QtCreator 2011-09-04T12:14:20
#
#-------------------------------------------------

QT       += core gui

TARGET = proyecto2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scene.cpp \
    item.cpp \
    chip.cpp \
    game.cpp \
    action.cpp \
    node.cpp

HEADERS  += mainwindow.h \
    scene.h \
    item.h \
    chip.h \
    game.h \
    action.h \
    node.h

RESOURCES += \
    proyecto2.qrc
