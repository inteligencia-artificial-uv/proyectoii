/*
  *Nombre archivo: mainwindow.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha creación: 20 Septiembre de 2011
  *Fecha última actualización: 17 Dicciembre de 2011
  *Universidad del Valle
*/

#include <QGraphicsView>
//#include <QWidget>
#include <QLabel>
#include <QMessageBox>
#include <QTextCodec>
#include <QHBoxLayout>
#include <QAction>
#include <QMenuBar>
#include <QDebug>

#include "mainwindow.h"
#include "scene.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(linuxCodec);

    createActions();
    createMenus();

    int rows = 6;
    int columns = 6;

    scene = new Scene(rows,columns,this);
    scene->setSceneRect(QRectF(0, 0, 600, 500));
    view = new QGraphicsView(scene);
    scene->drawGrid();/*el tablero inicial*/

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(view);

    QWidget *widget = new QWidget;
    widget->setLayout(layout);

    setCentralWidget(widget);
    setWindowTitle(tr("Inteligencia Artificial - Proyecto 2 - Algoritmo MiniMax - Chess Trainer"));

    game = new Game(rows,columns);

    connect(scene,SIGNAL(moveMade()),this,SLOT(cursorWait()));
    connect(game,SIGNAL(moveResponse(Action)),this,SLOT(cursorArrow(Action)));

    connect(scene,SIGNAL(getPossibleMoves(char)),game,SLOT(getPossibleMoves(char)));
    connect(game,SIGNAL(possibleMoves(QVector<QPoint>)),scene,SLOT(setPossibleMoves(QVector<QPoint>)));

    connect(scene,SIGNAL(moveMade()),this,SLOT(startGame()));
    connect(game,SIGNAL(moveResponse(Action)),scene,SLOT(moveMadeResponse(Action)));

    connect(scene,SIGNAL(verifyInCheckEndMin()),game,SLOT(inCheckEndMin()));
    connect(game,SIGNAL(inCheckEndMinResponse(bool)),scene,SLOT(inCheckEndMin(bool)));

    connect(scene,SIGNAL(verifyInCheckMin(Action)),game,SLOT(inCheckMin(Action)));
    connect(game,SIGNAL(inCheckMinResponse(bool)),scene,SLOT(inCheckMin(bool)));

}

MainWindow::~MainWindow()
{
    delete view;
    delete scene;
    delete exitAction;
    delete aboutAction;
    delete helpAction;
    delete beginnerLevelAction;
    delete amateurLevelAction;
    delete expertLevelAction;
    delete newGameAction;
    delete fileMenu;
    delete helpMenu;
    delete levelMenu;
    delete GameMenu;
}

/*
  *Método slot about: recibe signal de un QAction, lo que abre un dialogo mostrando información
    acerca del programa y sus desarrolladores
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::about()
{
    QMessageBox::about(this, tr("Acerca del proyecto 2 IA"),
                       tr("Chess Trainer<br><br>"
                           "<b>Desarrolladores:</b> <br><br>"
                          "Maria Andrea Cruz Blandon. - 0831816.<br>"
                          "Cristian Leonardo R&iacute;os L&oacute;pez. - 0842139.<br>"
                          "Yerminson Doney Gonzalez Mu&ntilde;oz. - 0843846.<br><br>"
                          "Desarrollado usando Qt 4.7.4 y C++."
                          ));
}

/*
*/
void MainWindow::amateurLevel(bool checked)
{
    if(checked)
    {
        beginnerLevelAction->setChecked(false);
        expertLevelAction->setChecked(false);
    }else
    {
        amateurLevelAction->setChecked(true);
    }

    game->setDepth(4);
}

/*
*/
void MainWindow::beginnerLevel(bool checked)
{
    if(checked)
    {
        amateurLevelAction->setChecked(false);
        expertLevelAction->setChecked(false);
    }else
    {
        beginnerLevelAction->setChecked(true);
    }

    game->setDepth(2);
}

void MainWindow::cursorWait()
{
    QCursor cursor(Qt::WaitCursor);
    this->setCursor(cursor);
}

void MainWindow::cursorArrow(Action a)
{
    QCursor cursor(Qt::ArrowCursor);
    this->setCursor(cursor);
}

/*
  *Método createActions: inicializa todos los atributos QAction que forman la interfaz,
    además de conectarlos con su respectivo slot de escucha de señales emitidas
  *Entradas: no recibe entradas
  *Salida: no tiene salida, ayuda en la construccion de la interfaz
*/
void MainWindow::createActions()
{
    exitAction = new QAction(tr("Salir"), this);
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

    aboutAction = new QAction(tr("Acerca de"), this);
    connect(aboutAction, SIGNAL(triggered()),
            this, SLOT(about()));

    helpAction = new QAction(tr("Ayuda"), this);
    connect(helpAction, SIGNAL(triggered()), this, SLOT(help()));

    beginnerLevelAction = new QAction(tr("Principiante"), this);
    beginnerLevelAction->setCheckable(true);
    beginnerLevelAction->setChecked(true);
    connect(beginnerLevelAction, SIGNAL(triggered(bool)), this, SLOT(beginnerLevel(bool)));

    amateurLevelAction = new QAction(tr("Aficionado"), this);
    amateurLevelAction->setCheckable(true);
    connect(amateurLevelAction, SIGNAL(triggered(bool)), this, SLOT(amateurLevel(bool)));

    expertLevelAction = new QAction(tr("Experto"), this);
    expertLevelAction->setCheckable(true);
    connect(expertLevelAction, SIGNAL(triggered(bool)), this, SLOT(expertLevel(bool)));

    newGameAction = new QAction(tr("Juego Nuevo"), this);
    connect(newGameAction, SIGNAL(triggered()), this, SLOT(newGame()));
}

/*
  *Método createMenus: inicializa todos los atributos QMenu que forman la interfaz,
    estos se construyen usando QAction
  *Entradas: no recibe entradas
  *Salida: no tiene salida, ayuda en la construccion de la interfaz
*/
void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("Archivo"));
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    levelMenu = new QMenu("Nivel");
    levelMenu->addAction(beginnerLevelAction);
    levelMenu->addAction(amateurLevelAction);
    levelMenu->addAction(expertLevelAction);

    helpMenu = menuBar()->addMenu(tr("Más"));
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(helpAction);

    GameMenu = menuBar()->addMenu(tr("Juego"));
    GameMenu->addAction(newGameAction);
    GameMenu->addSeparator();
    GameMenu->addMenu(levelMenu);

}

/*
*/
void MainWindow::expertLevel(bool checked)
{
    if(checked)
    {
        beginnerLevelAction->setChecked(false);
        amateurLevelAction->setChecked(false);
    }else
    {
        expertLevelAction->setChecked(true);
    }

    game->setDepth(6);
}

/*
  *Método slot help: recibe signal de un QAction, lo que abre un dialogo mostrando información
    relevanet a cerca del dfuncionamiento del programa(correcto uso)
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::help()
{

}

void MainWindow::newGame()
{
    game->clearStates();
    game->generateBoard();
    scene->newGame(game->getBoard(),game->getChips());
    scene->drawGrid();
    this->startGame();
}

void MainWindow::startGame()
{
    game->play();
}

void MainWindow::verifyInChekMin(QPoint position)
{
    //bool check = game->inCheck(position,Chip::Negro);
    //scene->setIsCheckMin(check);
}
