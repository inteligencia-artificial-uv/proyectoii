/*
 *Nombre archivo: chip.h
 *Autor: Cristian Leonardo Ríos López
 *Fecha creación: Septiembre de 2011
 *Fecha última actualización: Diciembre del 2011
 *Descripción: La clase Chip es la que represena una ficha del ajedrez como tal
 y por esta razon tiene como atributos  el color, el tipo,  la posicion donde se
 encuentra dentro del tablero y un id   que la identifica.
 Con respecto a los ids se maneja la siguiente semantica
 Letras mayusculas y numeros pares = Fichas Negras -> Exceptuando el cero que indica que la posicion esta libre.
 Letras minusculas y numeros impares = Fichas Blancas

 *Universidad del Valle
*/

#ifndef CHIP_H
#define CHIP_H

#include <QPoint>

class Chip
{
    public:
        enum color {Blanco,Negro};
        enum type {Peon,Alfil,Caballo,Reina,Rey,Torre};
        Chip();
        Chip(QPoint position, color myColor,type myType, char id);
        Chip(Chip *chip);
        void setPosition(QPoint position);
        void setColor(color c);
        void setType(type t);
        void setId(char id);
        QPoint getPosition();
        color getColor();
        type getType();
        char getId();

    private:
        QPoint position;
        color myColor;
        type myType;
        char id;
};

#endif // CHIP_H
