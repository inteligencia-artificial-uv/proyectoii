#include "node.h"

Node::Node(Node* dad,char** board, int width, int height,QVector<Chip> chips,type myType, int depth, Action action)
{
    this->dad = dad;
    this->board = board;
    this->width = width;
    this->height = height;
    this->chips = chips;
    this->myType = myType;
    this->depth = depth;
    this->action = action;
    this->isLeft = false;

    if(myType == Node::Max)
        utility = -1000000;
    else
        utility = 1000000;
}

Node::~Node()
{
    for(int i=0; i<this->height; i++)
    {
        delete this->board[i];
        this->board[i] = 0;
    }
    delete this->board;
    this->board = 0;

    //delete dad; //no se puede borrar al padre
    dad = 0;

    //delete next; no se puede borrar el siguiente, lo mas posible es que ya no exista
    next = 0;
}

void Node::setBoard(char** board)
{
    this->board = board;
}

void Node::setChips(QVector<Chip> chips)
{
    this->chips = chips;
}

void Node::setUtility(int utility)
{
    this->utility=utility;
}

void Node::setType(type myType)
{
    this->myType = myType;
}

void Node::setWidth(int width)
{
    this->width = width;
}

void Node::setHeight(int height)
{
    this->height = height;
}

void Node::setDad(Node* dad)
{
    this->dad = dad;
}

void Node::setDepth(int depth)
{
    this->depth = depth;
}

 void Node::setNext(Node *nextNode)
 {
     this->next = nextNode;
 }

 void Node::setAction(Action action)
 {
     this->action = action;
 }

 void Node::setIsLeft(bool isLeft)
 {
     this->isLeft = isLeft;
 }

//retorna una copia
char** Node::getBoard()
{
    char** copyBoard= new char*[this->height];
    for(int i=0; i< this->height; i++)
    {
        copyBoard[i]= new char[this->width];
    }

    for(int i=0; i<this->height; i++)
        for(int j=0; j<this->width; j++)
            copyBoard[i][j]= this->board[i][j];

    return copyBoard;
}

QVector<Chip> Node::getChips()
{
    return chips;
}

int Node::getUtility()
{
    return utility;
}

Node::type Node::getType()
{
    return myType;
}

int Node::getWidth()
{
    return width;
}

int Node::getHeight()
{
    return height;
}

Node* Node::getDad()
{
    return dad;
}

int Node::getDepth()
{
    return depth;
}

Node* Node::getNext()
{
    return next;
}

Action Node::getAction()
{
    return action;
}

bool Node::getIsLeft()
{
    return isLeft;
}

void Node::updateUtility(int utility, Node* who)
{
    switch(myType)
    {
        case Node::Max:
            if(utility > this->utility)
            {
                this->utility = utility;
            }
            break;
        case Node::Min:
            if(utility < this->utility)
            {
                this->utility = utility;
            }
    }
    next = who;
}

void Node::updateUtilityDad()
{
    dad->updateUtility(utility,this);
}
